'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Drink Schema
 */
var DrinkSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Veuillez saisir le nom de la boisson',
		trim: true
	},
	family: {
		type: String,
		required: 'Veuillez saisir une famille',
		trim: true
	},
	type: {
		type: String,
		default: 'soft',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	quantity: {
		type: Number,
		default:'',
		required: "Veuillez saisir une quantité"
	}
});

mongoose.model('Drink', DrinkSchema);