'use strict';

module.exports = {
	db: 'mongodb://127.0.0.1/bartender-dev',
	app: {
		title: 'Bartender - Development Environment'
	},
	google: {
		clientID: process.env.GOOGLE_ID || '866878096227-pv7aoom3renm23kb5h6abgp220e68068.apps.googleusercontent.com',
		clientSecret: process.env.GOOGLE_SECRET || 'xlfiYLNr2JeiQv0tsvcRL4Hf',
		callbackURL: '/auth/google/callback'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'MAILER_FROM',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
				pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
			}
		}
	}
};
