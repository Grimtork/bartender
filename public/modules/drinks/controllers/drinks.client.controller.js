'use strict';

// Drinks controller
angular.module('drinks').controller('DrinksController', ['$scope', '$stateParams', '$location', 'Authentication', 'Drinks',
	function($scope, $stateParams, $location, Authentication, Drinks) {
		$scope.authentication = Authentication;

		// Create new Drink
		$scope.create = function() {
			// Create new Drink object
			var drink = new Drinks ({
				name: 		this.name,
				family:		this.family,
				type: 		this.type,
				quantity: 	this.quantity
			});

			// Redirect after save
			drink.$save(function(response) {
				$location.path('drinks');

				// Clear form fields
				$scope.name = '';
				$scope.family = '';
				$scope.type = '';
				$scope.quantity = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Drink
		$scope.remove = function(drink) {
			if ( drink ) { 
				drink.$remove();

				for (var i in $scope.drinks) {
					if ($scope.drinks [i] === drink) {
						$scope.drinks.splice(i, 1);
					}
				}
			} else {
				$scope.drink.$remove(function() {
					$location.path('drinks');
				});
			}
		};

		// Update existing Drink
		$scope.update = function() {
			var drink = $scope.drink;
			drink.$update(function() {
				$location.path('drinks');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Drinks
		$scope.find = function() {
			$scope.drinks = Drinks.query();
		};

		$scope.increment = function(drink, number) {
			for (var i in $scope.drinks) {
				if ($scope.drinks [i] === drink) {
					$scope.drinks[i].quantity = $scope.drinks[i].quantity + number;
					var drink = $scope.drinks[i];
					drink.$update(function() {
						$scope.error = "Success!!!";
					}, function(errorResponse) {
						$scope.error = errorResponse.data.message;
					});
				}
			}
		};

		$scope.decrement = function(drink, number) {
			for (var i in $scope.drinks) {
				if ($scope.drinks [i] === drink) {
					$scope.drinks[i].quantity = $scope.drinks[i].quantity - number;
					var drink = $scope.drinks[i];
					drink.$update(function() {
						$scope.error = "Success!!!";
					}, function(errorResponse) {
						$scope.error = errorResponse.data.message;
					});
				}
			}
		};

		// Find existing Drink
		$scope.findOne = function() {
			$scope.drink = Drinks.get({ 
				drinkId: $stateParams.drinkId
			});
		};
	}
]);